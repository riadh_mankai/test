/*
* Controller: contains remote actions for the Table Tennis AngularJS application
* Brownbags #4
* Devenson Applications,  april 2014
*/
public with sharing class dvs_TableTennisController {
	public dvs_TableTennisController() {
		
	}

  //get all players with their total points
	@RemoteAction
    public static List<Player__c> getPlayers () {
        List<Player__c> l_playersList = [SELECT Id, Name, Total__c 
                   FROM Player__c
                   ];
        return l_playersList;
    }

  // get all matchs infos
  @RemoteAction
    public static List<Match__c> getMatchs () {
        List<Match__c> l_MatchsList = [SELECT Id, Player1__c, Player2__c, 
        Player1__r.Name, Player2__r.Name, Score1__c, Score2__c 
                   FROM Match__c
                   ];
        return l_MatchsList;
    }

  // set all matchs infos, update players total points
  @RemoteAction
    public static String setMatchs (List<Match__c> p_matchs) {
        System.debug('[setMatchs]input matchs = ' + p_matchs);
        List<Match__c> matchstoUpdateList = new List<Match__c>();
        
        Map<Id, Player__c> l_playersMap = new Map<Id, Player__c>([SELECT Id, Name, Total__c 
                   FROM Player__c]);

        for (Match__c m : p_matchs) {
            matchstoUpdateList.add(new Match__c(Id = m.Id, Score1__c = m.Score1__c, Score2__c = m.Score2__c));
            l_playersMap.get(m.Player1__c).Total__c += m.Score1__c;
            l_playersMap.get(m.Player2__c).Total__c += m.Score2__c;
        }
        String result = 'Update Successfull';
        try {
          update matchstoUpdateList;
          update l_playersMap.values();
        } catch (Exception e) {
          result = 'Error Update : ' + e.getMessage();
        }
        return result;
    }

}